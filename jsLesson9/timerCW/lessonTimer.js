//elements dial
const msElement = document.querySelector('.ms');
const secondElement = document.querySelector('.second');
const minuteElement = document.querySelector('.minute');
//buttons
const startButton = document.querySelector('.start');
const stopButton = document.querySelector('.stop');
const resetButton = document.querySelector('.reset');
//const resultButton = document.querySelector('.result');
//listeners
const selectStart = () => {
//startButton.addEventListener('click', () => {
    clearInterval(intervalHandler);
    intervalHandler = setInterval(startTimer, 10);
//});
};
stopButton.addEventListener('click', () => { clearInterval(intervalHandler) });
resetButton.addEventListener('click', () => {
    clearInterval(intervalHandler);
    clearReset();
});
function clearReset() {
    ms = 00;
    second = 00;
    minute = 00;
    msElement.textContent = '00';
    secondElement.textContent = '00';
    minuteElement.textContent = '00';
};
/*resultButton.addEventListener('click', () => {
    number++;
    clearInterval(intervalHandler);
    const results = document.querySelector('.results');
    const block = document.createElement('div');
    block.classList.add('results__info');
    block.innerText = `Result ${number} : ${minute}:${second}:${ms}`;
    results.append(block);
    clearReset();//reset dial
    clearInterval(intervalHandler);//clear interval
    intervalHandler = setInterval(startTimer, 10);//new start
});*/

//variables
let ms = 00;
let second = 00;
let minute = 00;
let intervalHandler;
//let number = 0;

//fn button start
const startTimer = () => {
    //ms
    ms++;
    if (ms < 9) { msElement.innerText = '0' + ms };
    if (ms > 9) { msElement.innerText = ms };
    if (ms > 99) {
        second++;
        secondElement.innerText = '0' + second;
        ms = 0;
        msElement.innerText = '0' + ms;
    };

    //second
    if (second < 9) { secondElement.innerText = '0' + second };
    if (second > 9) { secondElement.innerText = second };
    if (second > 59) {
        minute++;
        minuteElement.innerText = '0' + minute;
        second = 0;
        secondElement.innerText = '0' + second;
    };

    //minute
    if (minute < 9) { minuteElement.innerText = '0' + minute };
    if (minute > 9) { minuteElement.innerText = minute };
    if (minute > 59) {
        clearReset();
        clearInterval(intervalHandler);
    };
};
