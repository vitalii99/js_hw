window.addEventListener('DOMContentLoaded', () => {
    const divRate = document.querySelectorAll('.rate')

    // fn loading indicator display
    // function show() {
    // document.getElementById("loader").style.display = "block";
    // }

    // fn removing the indicator
    // function hide() {
    // document.getElementById("loader").style.display = "none";
    // }

    const xhr = new XMLHttpRequest();
    //console.log(xhr.readyState)

    // document.getElementById("btnGet").onclick = function () {

    xhr.open("GET", "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json", true);
    //console.log(xhr.readyState)

    xhr.onreadystatechange = function () {
        //console.log(xhr.readyState)

        if (xhr.readyState == 4 && xhr.status == 200) {
            // hide();
            var data = JSON.parse(xhr.responseText);
            //console.log(data)
            document.querySelector("#date").innerHTML = data[0].exchangedate;

            for (i = 0; i < data.length; i++) {
                switch (data[i].cc) {
                    case "EUR": {
                        divRate[0].innerHTML = data[i].rate;
                        break;
                    }
                    case "USD": {
                        divRate[1].innerHTML = data[i].rate;
                        break;
                    }
                    case "CAD": {
                        divRate[2].innerHTML = data[i].rate;
                        break;
                    }
                    case "GBP": {
                        divRate[3].innerHTML = data[i].rate;
                        break;
                    }
                    case "PLN": {
                        divRate[4].innerHTML = data[i].rate;
                        break;
                    }
                    case "CHF": {
                        divRate[5].innerHTML = data[i].rate;
                        break;
                    }
                    case "DKK": {
                        divRate[6].innerHTML = data[i].rate;
                        break;
                    }
                }
            }
        }
    }
    xhr.send();
    // show();
    // }
})