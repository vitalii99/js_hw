window.addEventListener('DOMContentLoaded', () => {
    const btnGet = document.querySelector("#btnGet")
    // fn loading indicator display
    function show() {
        document.querySelector("#loader").style.display = "block";
    }
    // fn removing the indicator
    function hide() {
        document.querySelector("#loader").style.display = "none";
    }
    const xhr = new XMLHttpRequest();
    // console.log(xhr.readyState)

    function addCurrency() {
        xhr.open("GET", "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json", true);
        //console.log(xhr.readyState)

        xhr.onreadystatechange = function () {
            // console.log(xhr.readyState)

            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    hide();
                    var data = JSON.parse(xhr.responseText);
                    data.splice(-4)
                    // console.log(data)

                    const table = document.querySelector('table');
                    table.style.visibility = "visible";

                    document.querySelector("caption>#date").innerHTML = data[0].exchangedate;
                    const filteredCurrencies = data.filter(elem => elem.rate >= 25);
                    // console.log(filteredCurrencies)

                    for (let i = 0; i < filteredCurrencies.length; i++) {
                        // filteredCurrencies.forEach((elem) => {
                        // if (elem.cc !== "XDR") {
                        if (filteredCurrencies[i].cc !== "XDR") {
                            const tbody = document.querySelector('table>tbody');
                            const createTr = document.createElement('tr');
                            tbody.append(createTr);

                            for (let i = 0; i < 3; i++) {
                                const createTd = document.createElement('td');
                                createTr.append(createTd);
                            }
                            // createTr.firstChild.innerHTML = elem.txt;
                            createTr.firstChild.innerHTML = filteredCurrencies[i].txt;
                            // createTr.firstChild.nextSibling.innerHTML = elem.cc;
                            createTr.firstChild.nextSibling.innerHTML = filteredCurrencies[i].cc;
                            // createTr.lastChild.innerHTML = elem.rate;
                            createTr.lastChild.innerHTML = filteredCurrencies[i].rate;
                        }
                        // })
                    }
                }
                else {
                    const p = document.querySelector('p');
                    // console.log(p)
                    const div = document.createElement("div")
                    p.after(div)
                    div.classList.add("error");
                    if (xhr.status == 400) {
                        document.querySelector(".error").innerHTML = "ERROR status code: 400"
                    }
                    if (xhr.status == 500) {
                        document.querySelector(".error").innerHTML = "ERROR status code: 500"
                    }
                }
            }
        }
        xhr.send();
        show();
        btnGet.removeEventListener('click', addCurrency);
    }
    btnGet.addEventListener('click', addCurrency);
    
})

