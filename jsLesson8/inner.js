document.body.onload = () => {
    const root1 = document.querySelector('#root'),

    p1 = document.createElement('p');
    p1.innerHTML = `USD 39.95 40.45`;
    root1.appendChild(p1);
    
    p2 = document.createElement('p');
    p2.innerHTML = `EUR 41.60 42.60`;
    root1.appendChild(p2);

    p3 = document.createElement('p');
    p3.innerHTML = `PLN&nbsp;&nbsp; 8.35 &nbsp;&nbsp; 9.25`;
    root1.appendChild(p3);

    p4 = document.createElement('p');
    p4.innerHTML = `GBP 44.38 46.29`;
    root1.appendChild(p4);
}