//IE11+ operamini no  dataset
window.addEventListener('load', () => {
    const calculator = document.querySelector('.calculator'); //звертаємось до батьківського елемента
    const keys = calculator.querySelector('.calc_keys'); //const keys = document.querySelector('.calc_keys');
    //звертаємось до display 2 const display = document.querySelector('.calc_display');
    const display = calculator.querySelector('.calc_display');
    const operatorKeys = keys.querySelectorAll('[data-type="operator"]');

    keys.addEventListener('click', event => {
        //блокуєм батьківський елемент вспливанням, не кнопки 1 
        if (!event.target.closest('button')) return; //console.log(event.target);
        const key = event.target; //елемент
        const keyValue = key.textContent; //значення //console.log(key.textContent);
        const displayValue = display.textContent; //console.log(displayValue);
        const { type } = key.dataset; //const type = key.dataset.type //змінна присвоїли значення для 3, 4 умов 
        //type assertion модель приведення значення до визначеного типу  const{value} значння у елементі
        const { previousKeyType } = calculator.dataset;
        //number key 3
        //if (key.classList.contains('btn_digit')) //if (key.dataset.type === 'digit')
        if (type === 'digit') {
            //конкатенація значень  //console.log('0')  //console.log(0)
            if (displayValue === '0' || previousKeyType === 'operator') {
                display.textContent = keyValue;
            } else {
                display.textContent = displayValue + keyValue; //console.log(display.textContent); 
            }
            //винесли в type 5 : calculator.dataset.previousKeyType = 'digit';// data атрибут display, попередній ключ(значення елемента) 123 конкатенація після знаку
        }
        //operator key 4
        //key.dataset.type === 'operator';//key.dataset.keyType === 'operator'; //key.getAttribute('data-key-type') === 'operator';
        //if (key.classList.contains('btn_operator')) //if (key.dataset.type === 'operator')
        if (type === 'operator') {
            //const operatorKeys = keys.querySelectorAll('[data-type="operator"]');
            operatorKeys.forEach(el => { el.dataset.state = '' });
            //або
            //const currentActiveOperator = calculator.querySelector('[data-state="selected"]');
            //if (currentActiveOperator){
            //currentActiveOperator.dataset.state = '';
            //}
            key.dataset.state = 'selected';
            calculator.dataset.firstDigit = displayValue;
            calculator.dataset.operator = key.dataset.key;
            //console.log(key); //значення
            //винесли в type 5 : calculator.dataset.previousKeyType = 'operator'; // data атрибут display, попередній ключ(значення елемента) 123 конкатенація до знаку
        }
        //equal виконуєм розрахунки 6 
        if (type === 'equal') {
            const firstDigit = calculator.dataset.firstDigit;
            const operator = calculator.dataset.operator;
            const secondDigit = displayValue;
            // =>
            //let result = '';
            //if (operator === 'plus') result = firstDigit + secondDigit;
            //if (operator === 'minus') result = firstDigit - secondDigit;
            //if (operator === 'times') result = firstDigit * secondDigit;
            //if (operator === 'divide') result = firstDigit / secondDigit;
            display.textContent = calculate(firstDigit, operator, secondDigit); //display.textContent = result;
        }

        if (type === 'reset') {
            display.textContent = '0';
            delete calculator.dataset.firstDigit;
            delete calculator.dataset.operator;
        }

        if (type === 'plus_minus') {
            display.textContent = displayValue * -1;
        }

        if (type === 'percent') {
            display.textContent = displayValue / 100;
        }
        // display 5
        calculator.dataset.previousKeyType = type;
    })

    function calculate(firstDigit, operator, secondDigit) {
        firstDigit = parseFloat(firstDigit);
        secondDigit = parseFloat(secondDigit);
        if (operator === 'plus') return firstDigit + secondDigit;//result = firstDigit + secondDigit;
        if (operator === 'minus') return firstDigit - secondDigit;
        if (operator === 'times') return firstDigit * secondDigit;
        if (operator === 'divide') return firstDigit / secondDigit;
    }












    //switch =>
    //let result = '';
    /*switch ('operator') {
        case 'plus': result = firstDigit + secondDigit; break;
        case 'minus': result = firstDigit - secondDigit; break;
        case 'times': result = firstDigit * secondDigit; break;
        case 'divide': result = firstDigit / secondDigit; break;
    }*/
    //return result;


})

