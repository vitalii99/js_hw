'use strict'
/** Створіть функцію кожний виклик якої буде генерувати випадкові числа від 1 до 100
але так, щоб вони не поверталися, поки не будуть перебрані усі числа із цього проміжку.
Вирішити задачу через замикання - в замиканні повинен зберігатися масив чисел, які вже
були згенеровані функцією 
*/

$(document).ready(function () {
    function myRandom() {
        let arr = [];

        return function generateNumber() {

            for (let i = 0; i < 600; i++) {

                let numb = Math.floor(Math.random() * 100 + 1);

                if (arr.indexOf(numb) === -1) {
                    arr.push(numb)
                    console.log(arr)
                }
            }
        }
    }

    let array = myRandom();

    $(array);

    array = null // очистка памяті
})
