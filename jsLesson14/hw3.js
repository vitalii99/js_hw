'use strict'
// Створіть функцію яка рахує та виводить кількість своїх викликів
$(document).ready(function () {
function myCounter() {
    let count = 0;
    // let count = 1; якщо (постпрефіксний інкремент)

    return function () { console.log(`Кількість викликів: ${++count}`) } // ++count (префіксний інкремент)
    // count++
}

const counter = myCounter(); // counter нове лексичне оточення порожнє є посиланням на зовнішнє оточення 
// outer яке надає доступ до попереднього виклику myCounter де counter був створений

// $(counter); // 0 [[Environment]] глобальний виклик при count = 0;
$(counter); // 1 поточний виклик
$(counter); // 2
$(counter);
$(counter);
$(counter);
})
